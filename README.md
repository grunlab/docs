[![pipeline status](https://gitlab.com/grunlab/docs/badges/main/pipeline.svg)](https://gitlab.com/grunlab/docs/-/commits/main)

# GrunLab Docs

docs.grunlab.net deployment on Kubernetes

Docs: https://docs.grunlab.net/apps/docs.md

Base image: [grunlab/mkdocs][mkdocs]

Format: docker

Supported architecture(s):
- arm64

[mkdocs]: <https://gitlab.com/grunlab/mkdocs>