#!/bin/bash

MKDOCS_POD=$(kubectl get pod -n docs | grep mkdocs | cut -d ' ' -f 1)

while true
do
  kubectl cp ./build/docs.grunlab.net ${MKDOCS_POD}:/var/lib/mkdocs
  sleep 5
done